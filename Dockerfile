FROM citusdata/citus:10.0.3

RUN localedef --alias-file=/usr/share/locale/locale.alias --charmap=UTF-8 --force --inputfile=pt_BR pt_BR.UTF-8 && \
    ln --force --symbolic --verbose /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
    echo 'America/Sao_Paulo' | tee /etc/timezone && \
    apt-get --yes --fix-missing update && \
    apt-get --yes --fix-missing install postgresql-contrib && \
    rm --force --recursive /var/lib/apt/lists/*

ENV LANG pt_BR.utf8
