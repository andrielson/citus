#!/bin/sh
TAG=br10.0.3-20210330;
docker buildx build --no-cache --pull --tag registry.gitlab.com/andrielson/citus:$TAG .;
docker image tag registry.gitlab.com/andrielson/citus:$TAG registry.gitlab.com/andrielson/citus:latest;
